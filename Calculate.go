//Deploy with $ gcloud functions deploy calculator --entry-point Calculate --runtime go111 --trigger-http
// $ gcloud functions deploy Calculate --memory=256MB --runtime go111 --trigger-http

package calculator

import (
	"encoding/json"
	"fmt"
	"strconv"
	"net/http"
)

func add(num1, num2 int) int {
	return num1 + num2
}

func subtract(num1, num2 int) int {
	return num1 - num2
}

func multiply(num1, num2 float64) float64 {
	return num1 * num2
}

func divide(num1, num2 float64) float64 {
	return num1 / num2
}


func Calculate(w http.ResponseWriter, r *http.Request) {
            // Set CORS headers for the preflight request
        if r.Method == http.MethodOptions {
                w.Header().Set("Access-Control-Allow-Origin", "*")
                w.Header().Set("Access-Control-Allow-Methods", "POST")
                w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
                w.Header().Set("Access-Control-Max-Age", "3600")
                w.WriteHeader(http.StatusNoContent)
                return
        }
        // Set CORS headers for the main request.
        w.Header().Set("Access-Control-Allow-Origin", "*")

    
	var op struct {
		Num1, Op, Num2 string     
	}
	if err := json.NewDecoder(r.Body).Decode(&op); err != nil {
		fmt.Fprint(w, "error")
		return
	}
	if op.Num1 == "" || op.Op == "" || op.Num2 == "" {
		fmt.Fprint(w, "Something missing here")
		return
	}

	num1, err := strconv.Atoi(op.Num1)
	if err != nil {
		fmt.Println("error:", err)
	}
	num2, err := strconv.Atoi(op.Num2)
	if err != nil {
		fmt.Println("error:", err)
	}

	switch op.Op {
		case "+":
			result := add(num1, num2)
			fmt.Fprint(w, result)
			return
		case "-":
			result := subtract(num1, num2)
			fmt.Fprint(w, result)
			return
		case "*":
			result := multiply(float64(num1), float64(num2))
			fmt.Fprint(w, result)
			return
		case "/":
			result := divide(float64(num1), float64(num2))
			fmt.Fprint(w, result)
			return
		default:
			fmt.Fprint(w, "Invalid operation selected. Please try again!")
			return
	}

	fmt.Fprint(w, op.Num1, op.Op, op.Num2)
}